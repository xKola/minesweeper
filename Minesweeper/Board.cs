﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Minesweeper
{
    public class Board
    {
        private readonly Vector2 _dimensions;

        private readonly int _bombsAmount;
        private readonly List<Bomb> _bombsList;

        private readonly Cell[,] _cells;

        public Board(Vector2 dimensions)
        {
            this._dimensions = dimensions;
        }

        public Vector2 Dimensions => _dimensions;

        public Board(Vector2 dimensions, int bombsAmount)
        {
            _dimensions = dimensions;
            _bombsAmount = bombsAmount;
            _bombsList = new List<Bomb>();
            _cells = new Cell[_dimensions.x, _dimensions.y];
        }

        public void Draw()
        {
            for (int j = 0; j < _dimensions.y; j++)
            {
                for (int i = 0; i < _dimensions.x; i++)
                {
                    //Console.Write($"({_cells[i, j].position.x};{_cells[i, j].position.y})  ");
                    if (!_cells[i, j].revealed)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        if (_cells[i, j].boardSign == "?")
                        {
                            Console.Write($"{_cells[i, j].boardSign} ");   
                        }
                        else
                        {
                            Console.Write("- ");
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = _cells[i, j].color;
                        Console.Write($"{_cells[i, j].boardSign} ");
                    }

                    if (i >= _dimensions.x - 1)
                    {
                        Console.WriteLine();
                    }

                    Console.ResetColor();
                }
            }
        }

        public void PopulateWithBombs()
        {
            var randomize = new Random();
            int x, y;

            for (int i = 0; i < _bombsAmount; i++)
            {
                do
                {
                    x = randomize.Next(0, _dimensions.x);
                    y = randomize.Next(0, _dimensions.y);
                    if (!(_cells[x, y] is Bomb))
                    {
                        _cells[x, y] = new Bomb(new Vector2(x, y));
                        break;
                    }
                } while (_cells[x, y] is Bomb);
            }

//            _cells[0, 0] = new Bomb("#", new Vector2(0, 0));
//            _cells[_dimensions.x - 1, 0] = new Bomb("#", new Vector2(_dimensions.x - 1, 0));
//            _cells[0, _dimensions.y - 1] = new Bomb("#", new Vector2(0, _dimensions.y - 1));
//            _cells[_dimensions.x - 1, _dimensions.y - 1] = new Bomb("#", new Vector2(_dimensions.x - 1, _dimensions.y - 1));
            Console.WriteLine($"Populating map with {_bombsAmount} bombs done!");
        }

        public void GenerateBoard()
        {
            Console.WriteLine("Generating board");
            for (int j = 0; j < _dimensions.y; j++)
            {
                for (int i = 0; i < _dimensions.x; i++)
                {
                    _cells[i, j] = new NormalCell(new Vector2(i, j));
                }
            }
        }

        public void CalculateAdjacent()
        {
            for (int j = 0; j < _dimensions.y; j++)
            {
                for (int i = 0; i < _dimensions.x; i++)
                {
                    if (_cells[i, j] is Bomb)
                    {
                        _cells[i, j].bombsAround = 0;
                        
                        Console.WriteLine($"Im now calculating {i};{j} bomb");
                        //The same line as bomb
                        if (i > 0 && _cells[i - 1, j] is NormalCell) _cells[i - 1, j].bombsAround++;
                        if (i < _dimensions.x - 1 && _cells[i + 1, j] is NormalCell) _cells[i + 1, j].bombsAround++;

                        //Line above bomb
                        if (i > 0 && j > 0 && _cells[i - 1, j - 1] is NormalCell) _cells[i - 1, j - 1].bombsAround++;
                        if (j > 0 && _cells[i, j - 1] is NormalCell) _cells[i, j - 1].bombsAround++;
                        if (i < _dimensions.x - 1 && j > 0 && _cells[i + 1, j - 1] is NormalCell)
                            _cells[i + 1, j - 1].bombsAround++;

                        //Line below bomb
                        if (i > 0 && j < _dimensions.y - 1 && _cells[i - 1, j + 1] is NormalCell)
                            _cells[i - 1, j + 1].bombsAround++;
                        if (j < _dimensions.y - 1 && _cells[i, j + 1] is NormalCell) _cells[i, j + 1].bombsAround++;
                        if (i < _dimensions.x - 1 && j < _dimensions.y - 1 && _cells[i + 1, j + 1] is NormalCell)
                            _cells[i + 1, j + 1].bombsAround++;
                    }
                }
            }
        }

        public void CalculateIndex(Vector2 position)
        {
            int i = position.x;
            int j = position.y;
            _cells[i, j].bombsAround = 0;
                        
            Console.WriteLine($"Im now calculating {i};{j} bomb");
            //The same line as bomb
            if (i > 0 && _cells[i - 1, j] is Bomb) _cells[i, j].bombsAround++;
            if (i < _dimensions.x - 1 && _cells[i + 1, j] is Bomb) _cells[i, j].bombsAround++;

            //Line above bomb
            if (i > 0 && j > 0 && _cells[i - 1, j - 1] is Bomb) _cells[i, j].bombsAround++;
            if (j > 0 && _cells[i, j - 1] is Bomb) _cells[i, j].bombsAround++;
            if (i < _dimensions.x - 1 && j > 0 && _cells[i + 1, j - 1] is Bomb)
                _cells[i, j].bombsAround++;

            //Line below bomb
            if (i > 0 && j < _dimensions.y - 1 && _cells[i - 1, j + 1] is Bomb)
                _cells[i, j ].bombsAround++;
            if (j < _dimensions.y - 1 && _cells[i, j + 1] is Bomb) _cells[i, j].bombsAround++;
            if (i < _dimensions.x - 1 && j < _dimensions.y - 1 && _cells[i + 1, j + 1] is Bomb)
                _cells[i, j].bombsAround++;
        }
        
        public List<Cell> GetAdjacentCells(Vector2 middle, bool includeSlant)
        {
            List<Cell> adjacentCells = new List<Cell>();

            //The same line as bomb
            if (middle.x > 0 && _cells[middle.x - 1, middle.y] is NormalCell) adjacentCells.Add(_cells[middle.x - 1, middle.y]);
            if (middle.x < _dimensions.x - 1 && _cells[middle.x + 1, middle.y] is NormalCell) adjacentCells.Add(_cells[middle.x + 1, middle.y]);

            //Line above bomb
            if (includeSlant && middle.x > 0 && middle.y > 0 && _cells[middle.x - 1, middle.y - 1] is NormalCell) adjacentCells.Add(_cells[middle.x - 1, middle.y - 1]);
            if (middle.y > 0 && _cells[middle.x, middle.y - 1] is NormalCell) adjacentCells.Add(_cells[middle.x, middle.y - 1]);
            if (includeSlant && middle.x < _dimensions.x - 1 && middle.y > 0 && _cells[middle.x + 1, middle.y - 1] is NormalCell)
                adjacentCells.Add(_cells[middle.x + 1, middle.y - 1]);

            //Line below bomb
            if (includeSlant && middle.x > 0 && middle.y < _dimensions.y - 1 && _cells[middle.x - 1, middle.y + 1] is NormalCell)
                adjacentCells.Add(_cells[middle.x - 1, middle.y + 1]);
            if (middle.y < _dimensions.y - 1 && _cells[middle.x, middle.y + 1] is NormalCell) adjacentCells.Add(_cells[middle.x, middle.y + 1]);
            if (includeSlant && middle.x < _dimensions.x - 1 && middle.y < _dimensions.y - 1 && _cells[middle.x + 1, middle.y + 1] is NormalCell)
                adjacentCells.Add(_cells[middle.x + 1, middle.y + 1]);

            return adjacentCells;
        }

        public void RevealPosition(Vector2 position)
        {
            var currentCell = _cells[position.x, position.y];
            
            if(currentCell.revealed || currentCell.boardSign == "?") return;
            
            currentCell.revealed = true;
            if(currentCell is NormalCell normalCell) normalCell?.CalculateColors();
            
            if(currentCell is Bomb || currentCell.bombsAround > 0)
                return;

            var adjacentCells = GetAdjacentCells(position, true);
            
            foreach (var cell in adjacentCells.Where(x => x is NormalCell))
            {
                if(cell.bombsAround == 0)
                    RevealPosition(cell.position);
                else
                {
                    cell.CalculateColors();
                    cell.boardSign = cell.bombsAround.ToString();
                }
                cell.revealed = true;
            }

        }

        public void MarkPosition(Vector2 position)
        {
            var cell = _cells[position.x, position.y];
            if(cell.revealed) return;

            if (!string.Equals(cell.boardSign, "?", StringComparison.Ordinal))
            {
                cell.boardSign = "?";   
            }
            else
            {
                CalculateIndex(position);
                if(cell is NormalCell)
                    cell.boardSign = cell.bombsAround.ToString();
                if (cell is Bomb)
                    cell.boardSign = "#";
            }
        }
    }
}