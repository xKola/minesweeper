﻿using System;

namespace Minesweeper
{
    public abstract class Cell
    {
        public string boardSign;
        public int bombsAround = 0;
        public ConsoleColor color = ConsoleColor.White;
        public bool revealed = false;
        public Vector2 position;

        public Cell(Vector2 position)
        {
            this.position = position;
        }

        public abstract void CalculateColors();
    }
}