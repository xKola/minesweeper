﻿using System;

namespace Minesweeper
{
    public class NormalCell : Cell
    {
        public NormalCell(Vector2 position) : base(position)
        {
            boardSign = "-";
            color = ConsoleColor.DarkGray;
        }

        public override void CalculateColors()
        {
            switch (bombsAround)
            {
                case 0:
                    //boardSign = " ";
                    break;
                case 1:
                    color = ConsoleColor.Magenta;
                    break;
                case 2:
                    color = ConsoleColor.Green;
                    break;
                case 3:
                    color = ConsoleColor.Red;
                    break;
                case 4:
                    color = ConsoleColor.Blue;
                    break;
                case 5:
                    color = ConsoleColor.Magenta;
                    break;
            }

            boardSign = bombsAround.ToString();
            if (bombsAround == 0) boardSign = " ";
        }
    }
}