﻿using System;

namespace Minesweeper
{
    public class Bomb : Cell
    {
        public Bomb(Vector2 position) : base(position)
        {
            boardSign = "#";
            color = ConsoleColor.DarkRed;
        }

        public override void CalculateColors()
        {
            color = ConsoleColor.DarkRed;
        }
    }
}