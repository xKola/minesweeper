﻿namespace Minesweeper
{
    public class CursorSelection
    {
        public Vector2 position, snap;
        public string sign = "@";

        public CursorSelection(Vector2 position, string sign, Vector2 snap)
        {
            this.position = position;
            this.sign = sign;
            this.snap = snap;
        }
    }
}