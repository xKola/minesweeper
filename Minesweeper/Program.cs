﻿using System;

namespace Minesweeper
{
    class Program
    {
        public enum GameState { Playing, Exit }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.CursorVisible = false;
            
            GameState gameState = GameState.Playing;
            
            var board = new Board(new Vector2(16, 16), 40);
            board.GenerateBoard();
            board.PopulateWithBombs();
            board.CalculateAdjacent();
            
            var cursor = new CursorSelection(new Vector2(0, 0), "@", new Vector2(2, 1));
            
            while (gameState == GameState.Playing)
            {
                Console.Clear();

                board.Draw();
                Console.SetCursorPosition(cursor.position.x, cursor.position.y);
                Console.Write(cursor.sign);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        gameState = GameState.Exit;
                        break;
                    case ConsoleKey.W:
                        if (cursor.position.y == 0) break;
                        cursor.position.y -= cursor.snap.y;
                        break;
                    case ConsoleKey.A:
                        if (cursor.position.x == 0) break;
                        cursor.position.x -= cursor.snap.x;
                        break;
                    case ConsoleKey.S:
                        if (cursor.position.y == board.Dimensions.y - 1) break;
                        cursor.position.y += cursor.snap.y;
                        break;
                    case ConsoleKey.D:
                        if (cursor.position.x / cursor.snap.x == board.Dimensions.x - 1) break;
                        cursor.position.x += cursor.snap.x;
                        break;
                    case ConsoleKey.Enter:
                        board.RevealPosition(new Vector2(cursor.position.x / cursor.snap.x, cursor.position.y));
                        break;
                    case ConsoleKey.O:
                        board.MarkPosition(new Vector2(cursor.position.x / cursor.snap.x, cursor.position.y));
                        break;
                }
            }
        }
    }
}